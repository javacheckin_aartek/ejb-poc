package myServelet;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;

/**
 * XDoclet-based session bean.  The class must be declared
 * public according to the EJB specification.
 *
 * To generate the EJB related files to this EJB:
 *		- Add Standard EJB module to XDoclet project properties
 *		- Customize XDoclet configuration for your appserver
 *		- Run XDoclet
 *
 * Below are the xdoclet-related tags needed for this EJB.
 * 
 * @ejb.bean name="ListElement"
 *           display-name="Name for ListElement"
 *           description="Description for ListElement"
 *           jndi-name="ejb/ListElement"
 *           type="Stateful"
 *           view-type="remote"
 */
@Stateful
public class ListElement implements SessionBean,ListElementRemote {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The session context */
	
	private List<Integer> values=new ArrayList<Integer>();
	private SessionContext context;

	public ListElement() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void ejbActivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ejbPassivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ejbRemove() throws EJBException, RemoteException {
		// TODO Auto-generated method stub

	}

	/**
	 * Set the associated session context. The container calls this method 
	 * after the instance creation.
	 * 
	 * The enterprise bean instance should store the reference to the context 
	 * object in an instance variable.
	 * 
	 * This method is called with no transaction context. 
	 * 
	 * @throws EJBException Thrown if method fails due to system-level error.
	 */
	public void setSessionContext(SessionContext newContext) throws EJBException {
		context = newContext;
	}

	/**
	 * An example business method
	 *
	 * @ejb.interface-method view-type = "remote"
	 * 
	 * @throws EJBException Thrown if method fails due to system-level error.
	 */
	public void replaceWithRealBusinessMethod() throws EJBException {
		// rename and start putting your business logic here
	}

	@Override
	public void addNum(int a) {
		values.add(a);
	}

	@Override
	public void removeNum(int a) {
		values.remove(new Integer(a));
	}

	@Override
	public List<Integer> fetchElement() {
		return values;
	}

}
