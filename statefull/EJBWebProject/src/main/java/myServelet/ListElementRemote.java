package myServelet;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface ListElementRemote {

	void addNum(int a);
	void removeNum(int a);
	
	List<Integer> fetchElement();
}
