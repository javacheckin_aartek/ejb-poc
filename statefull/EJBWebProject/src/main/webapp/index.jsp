<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.servlet.jsp.tagext.TryCatchFinally"%>
<%@page import="myServelet.ListElementRemote"%>
<%@page import="myServelet.ListElement"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%!
	private static ListElementRemote values;

	public void jspInit() {
		try {
			InitialContext  ic = new InitialContext();
			values = (ListElementRemote) ic.lookup("java:global/EJBWebProject-0.0.1-SNAPSHOT/ListElement");
		} catch (Exception e) {
		System.out.println("error is");
			e.printStackTrace();
		}
	}
	
	%>
 
 <%
 if(request.getParameter("Addnum")!=null)
 {
 if(request.getParameter("t1")!=null && !request.getParameter("t1").equals("")){
 int a=Integer.parseInt(request.getParameter("t1"));
 values.addNum(a);
 }
 }
 
  if(request.getParameter("RemoveNum")!=null)
  {
  	if(request.getParameter("t1")!=null && !request.getParameter("t1").equals("")){
 	int a=Integer.parseInt(request.getParameter("t1"));
    values.removeNum(a);
    }
  }
  %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>List manipulation Project</title>
  </head>
  
  <body>
  <form method="post">
  Enter the number: <input type="text" name="t1">
  <input type="submit" name="Addnum" value="Add">
  <input type="submit" name="RemoveNum" value="Remove">
  <% 
  if(values!=null)
  {
  List<Integer> valuelist=values.fetchElement();
  if(!valuelist.isEmpty()){
      for(Integer a:valuelist)
      {
      out.println("<br>"+a);
      }
       out.println("<br> size"+valuelist.size());
  }
  }
  %>
  </form>
  </body>
</html>
