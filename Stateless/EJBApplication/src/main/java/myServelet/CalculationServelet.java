package myServelet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myBean.CalculationBean;

/**
 * Servlet implementation class CalculationServelet
 */
@WebServlet("/CalculationServelet")
public class CalculationServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CalculationServelet() {
        super();
        // TODO Auto-generated constructor stub
    }

	@EJB
	CalculationBean calObj;//no dependancy to create the object EJB will handle it
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out =response.getWriter();
		int first=Integer.parseInt(request.getParameter("firstnumber"));
		int second=Integer.parseInt(request.getParameter("secondnumber"));
	
		calObj.setI(first);
		calObj.setJ(second);
		calObj.addData();
		calObj.subData();
		calObj.mulData();
		out.println("Calculator Result");
		out.println("The addition of two numbers is "+calObj.getAdd());
		out.println("The subtraction of two numbers is "+calObj.getSub());
		out.println("The multiplication of two numbers is "+calObj.getMul());
	}

}
