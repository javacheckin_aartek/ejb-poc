package myBean;

import java.rmi.RemoteException;

import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

/**
 * XDoclet-based session bean.  The class must be declared
 * public according to the EJB specification.
 *
 * To generate the EJB related files to this EJB:
 *		- Add Standard EJB module to XDoclet project properties
 *		- Customize XDoclet configuration for your appserver
 *		- Run XDoclet
 *
 * Below are the xdoclet-related tags needed for this EJB.
 * 
 * @ejb.bean name="Calculation"
 *           display-name="Name for Calculation"
 *           description="Description for Calculation"
 *           jndi-name="ejb/Calculation"
 *           type="Stateless"
 *           view-type="local"
 */
@Local
@Stateless
public class CalculationBean implements SessionBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer i,j,add,sub,mul;
	
	public Integer getI() {
		return i;
	}

	public void setI(Integer i) {
		this.i = i;
	}

	public Integer getJ() {
		return j;
	}

	public void setJ(Integer j) {
		this.j = j;
	}

	public Integer getAdd() {
		return add;
	}

	public void setAdd(Integer add) {
		this.add = add;
	}

	public Integer getSub() {
		return sub;
	}

	public void setSub(Integer sub) {
		this.sub = sub;
	}

	public Integer getMul() {
		return mul;
	}

	public void setMul(Integer mul) {
		this.mul = mul;
	}

	public void addData() {
		add=i+j;
	}
	public void subData() {
		sub=i-j;
	}
	public void mulData() {
		mul=i*j;
	}
	public SessionContext getContext() {
		return context;
	}

	public void setContext(SessionContext context) {
		this.context = context;
	}

	/** The session context */
	private SessionContext context;

	public CalculationBean() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void ejbActivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ejbPassivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ejbRemove() throws EJBException, RemoteException {
		// TODO Auto-generated method stub

	}

	/**
	 * Set the associated session context. The container calls this method 
	 * after the instance creation.
	 * 
	 * The enterprise bean instance should store the reference to the context 
	 * object in an instance variable.
	 * 
	 * This method is called with no transaction context. 
	 * 
	 * @throws EJBException Thrown if method fails due to system-level error.
	 */
	public void setSessionContext(SessionContext newContext) throws EJBException {
		context = newContext;
	}

	/**
	 * An example business method
	 *
	 * @ejb.interface-method view-type = "local"
	 * 
	 * @throws EJBException Thrown if method fails due to system-level error.
	 */
	public void replaceWithRealBusinessMethod() throws EJBException {
		// rename and start putting your business logic here
	}

}
