package newejbprojectusingcli;

import javax.ejb.Local;

/**
 * NewEJBProjectUsingCLI local interface
 */

public interface NewEJBProjectUsingCLILocal {

	  int add(int a, int b);
	  
	    int subtract(int a, int b);
	    
}