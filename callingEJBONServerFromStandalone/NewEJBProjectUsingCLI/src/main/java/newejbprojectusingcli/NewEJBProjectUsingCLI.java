package newejbprojectusingcli;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class NewEJBProjectUsingCLI
 */
@Stateless
@Remote(NewEJBProjectUsingCLILocal.class)
public class NewEJBProjectUsingCLI implements NewEJBProjectUsingCLILocal {


	 @Override
	    public int add(int a, int b) {
	        return a + b;
	    }
	 
	    @Override
	    public int subtract(int a, int b) {
	        return a - b;
	    }
}